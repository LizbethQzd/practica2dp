'use strict';

module.exports= (sequelize, DataTypes) => {
    const noticia = sequelize. define('noticia', {
        titulo:{type: DataTypes.STRING(150), defaultValue: "NONE"},
        archivo:{type: DataTypes.STRING(150), defaultValue: "NONE"},
        tipo_archivo:{type: DataTypes.ENUM(['VIDEO','IMAGEN']),defaultValue: "IMAGEN"},
        estado:{type: DataTypes.BOOLEAN, defaultValue: true},
        tipo_noticia:{type: DataTypes.ENUM(['NORMAL','FLASH','DEPORTIVA','POLITICA','CULTURAL','CIENTIFICA']),defaultValue: "NORMAL"},
        texto:{type: DataTypes.STRING(150), defaultValue: "NONE"},
        fecha:{type: DataTypes.DATEONLY},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},


    },{timestamps: false, freezeTableName: true});
    //asociaciones 
    noticia.associate = function(models){
        //relacion con persona
        noticia.belongsTo(models.persona, {foreignKey: 'id_persona'});

    };
    return noticia;
}