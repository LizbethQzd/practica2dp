'use strict';
var models = require('../models')
var persona = models.persona;
var noticia = models.noticia;
var fs = require('fs');
var extensiones = ['jpg'];
var formidable = require('formidable');
class NoticiaControl {
    //para listar
    async listar(req, res) {
        var lista = await noticia.findAll({
            include: [

                { model: models.persona, as: "persona", attributes: ['apellidos', 'nombres'] },
            ],
            attributes: ['titulo', ['external_id', 'id'], 'texto', 'cuerpo', 'tipo_archivo', 'fecha', 'tipo_noticia', 'archivo', 'estado']
        });
        //codigos de respuesta http
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    //para obtener
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await noticia.findOne({
            where: { external_id: external },
            include: [

                { model: models.persona, as: "persona", attributes: ['apellidos', 'nombres'] },
            ],
            attributes: ['titulo', ['external_id', 'id'], 'texto', 'cuerpo', 'tipo_archivo', 'fecha', 'tipo_noticia', 'archivo', 'estado']
        });
        //controles
        if (lista === undefined || lista == null) {
            //codigos de respuesta http
            res.status(200);
            res.json({ msg: "OK", code: 200, datos: {} });

        } else {
            //codigos de respuesta http
            res.status(200);
            res.json({ msg: "OK", code: 200, datos: lista });

        }
    }

    async guardar(req, res) {

        if (req.body.hasOwnProperty('titulo') &&
            req.body.hasOwnProperty('cuerpo') &&
            req.body.hasOwnProperty('tipo_noticia') &&
            req.body.hasOwnProperty('persona') &&
            req.body.hasOwnProperty('fecha')) {

            var uuid = require('uuid');

            var per_aux = await persona.findOne({
                where: { external_id: req.body.persona },
                include: [
                    { model: models.rol, as: "rol", attributes: [nombre] }]
            });

            if (per_aux === undefined || per_aux === null) {
                res.status(401);
                res.json({ msg: "Error", tag: "No se encuentra el editor", code: 401 });


            } else {
                var data = {
                    titulo: req.body.titulo,
                    cuerpo: req.body.cuerpo,
                    fecha: req.body.fecha,
                    tipo_noticia: req.body.tipo_noticia,
                    id_persona: per_aux.id,
                    archivo: "noticia.png",
                    estado: false,
                    external_id: uuid.v4(),

                };

                if (per_aux.rol.nombre === "editor") {
                    var result = await noticia.create(data);

                    if (result === null) {
                        res.status(401);
                        res.json({ msg: "Error", tag: "No se pudo crear", code: 401 });
                    } else {
                        per_aux.external_id = uuid.v4();
                        await per_aux.save();
                        res.status(200);
                        res.json({ msg: "OK", code: 200 });
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "Error", tag: "La persona que esta guardando la noticia no es un editor", code: 400 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }

    }

    //subir archivos formidable 
    //maimo se debe subir 2 megas reconocer si es imagen o video
    //imagenes png, jpg y  videos mp4
    /*async guardarFoto(req, res) {

        var form = new formidable.IncomingForm(), files = [];
        form.on('file', function (filed, file) {
            files.push(file);

        }).on('end', function () {
            console.log('ok');
        });

        //es asincrono no sincrono
        form.parse(req, function (err, fields) {
            let listado = files;
            let external = fields.external[0];
            //console.log();

            //let body =JSON.parse(fields, fields);
            for (let index = 0; index < listado.length; index++) {
                var file = listado(index);
                var extenseion = file.originalFilename.split('.').pop().toLowerCase();

                //console.log(body);
                if (extensiones.includes(extenseion)) {
                    const name = external + '.' + extenseion;
                    console.log(extenseion);
                    fs.rename(file.filepath, "public/multimedia" + name, async function (err) {
                        if (err) {
                            res.status(200);
                            res.json({ msg: "ERROR", tag: "No se pudo guardar el archivo", code: 200 });

                        } else {
                            res.status(200);
                            res.json({ msg: "OK", tag: "Archivo guardado", code: 200 });


                        }
                    });
                } else {
                    res.status(400);
                    res.json({ msg: "ERROR", tag: "Solo soporta"+extensiones, code: 400 });
                }


            }
            //res.status(200);
            //res.json({ msg: "OK", tag: "OK", code: 200 });

        });
    }*/

    async guardarFoto(req, res) {
        var form = new formidable.IncomingForm(), files = [];
        form.on('file', function (field, file) {
            files.push(file);
        }).on('end', function () {
            console.log('OK');
        });
        form.parse(req, async function (err, fields) {
            let listado = files;
            let nameArchivo = fields.nameArchivo[0];
            let external = fields.external[0];
            console.log(nameArchivo);
            console.log(external);
            var noti = await noticia.findOne({ where: { external_id: external } });
            if (noti === null) {
                console.log("valio");
                res.status(400);
                res.json({
                    msg: "No existe registro",
                    code: 400
                });
            } else {
                for (let index = 0; index < listado.length; index++) {
                    var file = listado[index];
                    var extenseion = file.originalFilename.split('.').pop().toLowerCase();
                    if (extensiones.includes(extenseion)) {
                        const name = nameArchivo + '.' + extenseion;
                        console.log(extenseion);
                        fs.rename(file.filepath, "public/multimedia/" + name, async function (error) {
                            if (error) {
                                res.status(400);
                                res.json({ msg: 'error', tag: "nose pudo guardar", code: 400 });
                            } else {
                                var uuid = require('uuid');
                                noti.archivo = name,
                                noti.external_id = uuid.v4();
                                var result = await noti.save();
                                if (result === null) {
                                    res.status(400);
                                    res.json({
                                        msg: "No se ha modificado sus datos",
                                        code: 400
                                    });
                                } else {
                                    res.status(200);
                                    res.json({
                                        msg: "Se ha modificado sus datos",
                                        code: 200
                                    });
                                }
                            }
                        });
                    } else {
                        res.status(400);
                        res.json({ msg: 'error', tag: "solo soporta png y jpg", code: 400 });
                    }
                }
            }
        });
    }

    async modificar(req, res) {
        console.log("ENTRO EN EL METODO")
        var newNoti = await noticia.findOne({ where: { external_id: req.body.external_id } });
        if (newNoti === null) {
            console.log("valio");
            res.status(400);
            res.json({
                msg: "No existe registro",
                code: 400
            });
        } else {
                var uuid = require('uuid');
                newNoti.cuerpo = req.body.cuerpo,
                newNoti.titulo = req.body.titulo,
                newNoti.fecha = req.body.fecha,
                newNoti.tipo_noticia = req.body.tipo_noticia,
                newNoti.external_id = uuid.v4();
                var result = await newNoti.save();
                if (result === null) {
                    res.status(400);
                    res.json({
                        msg: "No se ha modificado sus datos",
                        code: 400
                    });
                } else {
                    res.status(200);
                    res.json({
                        msg: "Se ha modificado sus datos",
                        code: 200
                    });
                }
        }
    }

    


}


module.exports = NoticiaControl;