'use strict';
module.exports = (sequelize, DataTypes) => {
    const venta = sequelize.define('venta', {
        // Definición de campos
        fechaventa:{type: DataTypes.STRING(150), defaultValue: "NONE"},
        descripcion:{type: DataTypes.STRING(150), defaultValue: "NONE"},
        monto:{type: DataTypes.STRING(20), defaultValue: "NONE"},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
    }, { freezeTableName: true });

    venta.associate = function(models) {
        venta.belongsTo(models.persona, { foreignKey: 'persona_id', as: 'persona' });
        venta.belongsTo(models.libro, { foreignKey: 'id_libro', as: 'libro' });
        //Venta.belongsTo(models.Comprador, { foreignKey: 'compradorId', as: 'comprador' });
    };

    return venta;
};
