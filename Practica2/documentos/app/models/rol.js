'use strict';

module.exports= (sequelize, DataTypes) => {
    const rol = sequelize. define('rol', {
        nombre:{type: DataTypes.STRING(100)},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},


    },{timestamps: false, freezeTableName: true});
    //asociaciones 
    rol.associate = function(models){
        //rol tiene muchas personas asociadas --relacion unidireccional
        rol.hasMany(models.persona, {foreignKey: 'id_rol', as: 'persona'});
    };
    return rol;
}