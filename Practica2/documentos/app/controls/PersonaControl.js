'use strict';
var models = require('../models')
var persona = models.persona;
var rol = models.rol;
class PersonaControl {
    //para listar
    async listar(req, res) {
        var lista = await persona.findAll({
            include: [

                { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
                { model: models.rol, as: "rol", attributes: ['nombre'] },
            ],
            attributes: ['apellidos', ['external_id', 'id'], 'nombres', 'direccion', 'celular', 'fecha_nac']

        });
        //codigos de respuesta http

        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }
    //obtener por external
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external },
            include: [

                { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
                { model: models.rol, as: "rol", attributes: ['nombre'] },
            ],
            attributes: ['apellidos', ['external_id', 'id'], 'nombres', 'direccion', 'celular', 'fecha_nac']

        });
        if (lista === undefined || lista== null) {
            //codigos de respuesta http
            res.status(200);
            res.json({ msg: "OK", code: 200, datos: {} });

        } else {
            //codigos de respuesta http
            res.status(200);
            res.json({ msg: "OK", code: 200, datos: lista });

        }

    }

    async guardar(req, res) {
        if (req.body.hasOwnProperty('nombres') &&
            req.body.hasOwnProperty('apellidos') &&
            req.body.hasOwnProperty('direccion') &&
            req.body.hasOwnProperty('celular') &&
            req.body.hasOwnProperty('fecha_nac') &&
            req.body.hasOwnProperty('correo') &&
            req.body.hasOwnProperty('clave') &&
            req.body.hasOwnProperty('rol')) {
            var uuid = require('uuid');
            var rolAux = await rol.findOne({ where: { external_id: req.body.rol } });
            if (rolAux != undefined) {
                var data = {
                    nombres: req.body.nombres,
                    apellidos: req.body.apellidos,
                    direccion: req.body.direccion,
                    celular: req.body.celular,
                    fecha_nac: req.body.fecha,
                    id_rol: rolAux.id,
                    external_id: uuid.v4(),
                    cuenta: {
                        correo: req.body.correo,
                        clave: req.body.clave
                    }
                }
                let transaction = await models.sequelize.transaction();
                try {
                    var result = await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                    await transaction.commit();
                    if (result === null) {
                        res.status(401);
                        res.json({ msg: "ERROR", tag: "No se puede crear", code: 401 });
                    } else {
                        //uuid.v4()
                        rolAux.external_id = uuid.v4();
                        await rolAux.save();
                        res.status(200);
                        res.json({ msg: "OK", code: 200 });
                    }
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    res.status(203);
                    res.json({ msg: "Error", code: 203, error_msg: error });
                }

            } else {
                res.status(400);
                res.json({ msg: "ERROR", tag: "El dato a buscar no existe", code: 400 });
            }

        } else {
            res.status(400);
            res.json({ msg: "ERROR", tag: "Faltan datos", code: 400 });
        }

    }

    async modificar(req, res) {
        console.log("ENTRO EN EL METODO")
        var person = await persona.findOne({ where: { external_id: req.body.external } });
        if (person === null) {
            console.log("valio");
            res.status(400);
            res.json({
                msg: "No existe registro",
                code: 400
            });
        } else {
            var uuid = require('uuid');
            person.nombres = req.body.nombres,
            person.apellidos = req.body.apellidos,
            person.direccion = req.body.direccion,
            person.celular = req.body.celular,
            person.fecha_nac = req.body.fecha_nac,
            person.external = uuid.v4();
            var result = await person.save();
            if (result === null) {
                res.status(400);
                res.json({
                    msg: "No se ha modificado sus datos",
                    code: 400
                });
            } else {
                res.status(200);
                res.json({
                    msg: "Se ha modificado sus datos",
                    code: 200
                });
            }
        }
    }
    
}

module.exports = PersonaControl;