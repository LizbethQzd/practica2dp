var express = require('express');
var router = express.Router();
let jwt = require('jsonwebtoken');

const personaC = require('../app/controls/PersonaControl');
let personaControl = new personaC();
const rolC = require('../app/controls/RolControl');
let rolControl = new rolC();
const noticiaC = require('../app/controls/NoticiaControl');
let noticiaControl = new noticiaC();
const cuentaC = require('../app/controls/CuentaControl');
let cuentaControl = new cuentaC();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('estoy todo bien');
});
//middleware
//validar el rol
const auth = function middleware(req, res, next) {
  const token = req.headers['news-token'];
  if (token === undefined) {
    res.status(401);
    res.json({ msg: "ERROR", tag: "Falta Token", code: 401 });
  } else {
    require('dotenv').config();
    const key = process.env.KEY_JWT;
    jwt.verify(token, key, async (err, decoded) => {
      if (err) {
        res.status(401);
        res.json({ msg: "ERROR", tag: "Token no valido o ha expirado", code: 401 });
      } else {
        console.log(decoded.external);
        const models = require('.../app/models');
        const cuenta = models.cuenta;
        const aux = await cuenta.findOne({
          where: { external_id: decoded.external }
        });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "ERROR", tag: "Token no valido", code: 401 });
        } else {
          //TODO...
          //autorizacion
          next();
        }

      }

    });

  }
  //console.log(token);
  //console.log(next);
  //console.log(req.url);

}
//inicio de sesion
router.post('/login', cuentaControl.inicio_sesion);
//el login no requiere auth
//api de personas
router.get('/admin/personas',auth, personaControl.listar);
router.post('/admin/personas/guardar', personaControl.guardar);
router.post('/admin/personas/modificar', personaControl.modificar);
router.get('/admin/personas/buscar/:external', personaControl.obtener);
//router.post('/admin/personas/modificar/:external', personaControl.modificar);

//api de rol
router.get('/admin/rol', rolControl.listar);
router.post('/admin/rol/guardar', rolControl.guardar);

//noticias
router.get('/noticias', noticiaControl.listar);
router.get('/noticias/buscar/:external', noticiaControl.obtener);
router.post('/admin/noticias/guardar', noticiaControl.guardar);

router.post('/admin/noticias/modificar', noticiaControl.modificar);
//router.post('/admin/noticias/archivo', noticiaControl.guardarFoto);
router.post('/admin/noticias/guardar/file', noticiaControl.guardarFoto);

module.exports = router;